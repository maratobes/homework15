#include <iostream>
using namespace std;

void PrintNums(int N, bool odd){
    int i = 0;
    if(odd) i=1;
    for(i; i<=N; i+=2)
        cout << i << " ";
}

int main() {
    int N;
    cout << "Enter N = ";
    cin >> N;
    cout << "\nOdd numbers from 0 to N: ";
    PrintNums(N, true);

    cout << "\nEven numbers from 0 to N: ";
    PrintNums(N, false);

    return 0;
}